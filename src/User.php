<?php
// Le dossier `src/` représente le namespace `App` (cf la clé "autoload" dans le fichier composer.json)
namespace App;

// les classes StringLengthException et EmailFormatException héritent toutes deux de la classe Esception, de cette manière, il est possible de créer des types d'exceptions différents.
class StringLengthException extends \Exception {}
class EmailFormatException extends \Exception {}

//La classe User représente un utilisateur dans notre application, elle peut être instanciée en appelant le constructeur (avec l'instruction `new User`) mais il est également possible d'appeler la méthode statique `User::getAll()` qui retourne un tableau d'instances de la classe User.
class User {
  public $name;
  public $surname;
  public $birthdate;
  public $email;
  public $password;

  public function __construct(string $name, string $surname, \DateTime $birthdate, string $email, string $password) {

    $this->validateStringLength($name, 2, "Le nom doit contenir au moins 2 caractères");
    $this->validateStringLength($surname, 2, "Le prénom doit contenir au moins 2 caractères");
    $this->validateStringLength($password, 8, "Le mot de passe doit contenir au moins 8 caractères");
    $this->validateEmail($email, "email invalide");

    $this->name = $name;
    $this->surname = $surname;
    $this->birthdate = $birthdate;
    $this->email = $email;
    $this->password = $password;
  }

  public function __toString(): string
  {
    return "Bonjour, je m'appelle {$this->name} {$this->surname}";
  }

  public static function getAll(array $toCreate): array
  {
    $output = [];
    foreach ($toCreate as $userToCreate) {
      $birthdate = new \DateTime($userToCreate["birthdate"]);

      $user = new User($userToCreate["name"], $userToCreate["surname"], $birthdate, $userToCreate["email"], $userToCreate["password"]);

      array_push($output, $user);
    }
    return $output;
  }

  private function validateStringLength(string $string, int $minLength, string $errMsg): void {
    if (strlen($string) < $minLength) {
      throw new StringLengthException($errMsg);
    }
  }

  private function validateEmail(string $mail, string $errMsg): void {
    if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
      throw new EmailFormatException($errMsg);
    }
  }
}