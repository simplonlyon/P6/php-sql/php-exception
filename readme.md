# Quickstart
To run this project you need : docker, docker-compose and composer installed.

1. run `composer install`
1. run `docker-compose up`
1. go to [http://localhost:8080](http://localhost:8080)